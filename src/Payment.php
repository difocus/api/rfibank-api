<?php
namespace ShopExpress\RfiBank;

use DateTime;
use ShopExpress\RfiBank\Injector\LoggerInjector;

final class Payment
{
    use LoggerInjector;

    const ERROR_ARGUMENT_VALUE = 'Parameter `%s` can\'t be empty';

    private $name;
    private $comment;
    private $cost;
    private $buttonText = 'Оплатить';
    private $buttonClass;
    private $baseUrl = 'https://partner.rficb.ru/alba/input/';
    private $secret;
    private $serviceId;
    private $orderId = 0;
    private $email;

    private $albaClient;

    private $data;

    /**
     *
     * @param int $serviceId The service identifier
     * @param string $secret The secret
     * @param string $baseUrl The base url
     */
    public function __construct(
        $serviceId,
        $secret,
        $baseUrl = null
    ) {
        $this->serviceId = $serviceId;
        $this->secret = $secret;
        if (!is_null($baseUrl) && !empty($baseUrl)) {
            $this->baseUrl = $baseUrl;
        }

        $this->albaClient = new \AlbaService($this->serviceId, $this->secret);
    }

    /**
     * Sets the name.
     *
     * @param string $name The name
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setName($name)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'name'));
        }

        $this->name = $name;

        return $this;
    }

    /**
     * Sets the comment.
     *
     * @param string $comment The comment
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setComment($comment)
    {
        if (empty($comment)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'comment'));
        }

        $this->comment = $comment;

        return $this;
    }

    /**
     * Sets the cost.
     *
     * @param float $cost The cost
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setCost($cost)
    {
        if (empty($cost) && $cost > 0) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'cost'));
        }

        $this->cost = $cost;

        return $this;
    }


    /**
      * Sets the order identifier.
      *
      * @param int $orderId The order identifier
      *
      * @throws \InvalidArgumentException
      *
      * @return self
      */
    public function setOrderId($orderId)
    {
        if (empty($orderId)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'orderId'));
        }

        $this->orderId = $orderId;

        return $this;
    }

   
    /**
     * Sets the email.
     *
     * @param string $email The email
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setEmail($email)
    {
        if (empty($email)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'email'));
        }

        $this->email = $email;

        return $this;
    }

    /**
     * Sets the button text.
     *
     * @param string $buttonText The button text
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setButtonText($buttonText)
    {
        if (empty($buttonText)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'buttonText'));
        }
        
        $this->buttonText = $buttonText;

        return $this;
    }

    /**
     * Sets the button class.
     *
     * @param string $buttonClass The button class
     *
     * @return self
     */
    public function setButtonClass($buttonClass)
    {
        $this->buttonClass = $buttonClass;

        return $this;
    }

    /**
     * Gets the payment form.
     *
     * @throws \Exception
     *
     * @return string The payment form HTML code.
     */
    public function getPaymentForm()
    {
        if (empty($this->cost)) {
            throw new \Exception('Error `cost` can\'t be empty');
        }

        if (empty($this->name)) {
            throw new \Exception('Error `name` can\'t be empty');
        }

        if (empty($this->serviceId)) {
            throw new \Exception('Error `serviceId` can\'t be empty');
        }

        $fields = [
            'cost' => $this->cost,
            'name' => $this->name,
            'service_id' => $this->serviceId,
            'version' => '2.0'
        ];

        if ($this->orderId !== false) {
            $fields['order_id'] = $this->orderId;
        }

        if (!empty($this->comment)) {
            $fields['comment'] = $this->comment;
        }

        if (!empty($this->email)) {
            $fields['email'] = $this->email;
        }

        $fields['check'] = $this->albaClient->sign('POST', $this->baseUrl, $fields, $this->secret);

        $string = "<form class='rbi-payment-button' action='{$this->baseUrl}' method='POST' accept-charset='UTF-8'>";

        foreach ($fields as $name => $field) {
            $string .= "<input type='hidden' name='{$name}' value='{$field}' />";
        }

        $string .=     "<button type='submit' class='{$this->buttonClass}'>{$this->buttonText}</button>";
        $string .= '</form>';

        return $string;
    }

    /**
     * Parse payment reponse input from RFIbank server.
     *
     * @return array The data array from RFIbank
     */
    public function parseInput()
    {
        $this->data = $_POST;

        if (!count($this->data)) {
            $this->getLogger()->error('Error `POST` array can\'t be empty', [$this->data]);
            return false;
        }

        $this->getLogger()->info('content', [$this->data]);

        return $this->data;
    }

    /**
     * Validate payment request from RFIbank server.
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function validate()
    {
        if (empty($this->cost)) {
            $this->getLogger()->error('Error `cost` can\'t be empty', [$this->data]);
            return false;
        }

        if ($this->albaClient->checkCallbackSign($this->data)) {
            if ($this->data['system_income'] == $this->cost) {
                $this->getLogger()->info('Success payment!');
                return true;
            } else {
                $this->getLogger()->error("Received `cost` {$this->data['system_income']} mismatch {$this->cost}", [$this->data]);
                return false;
            }
        }

        $this->getLogger()->info('Fail payment: signature mismatch!', [$this->data]);
        return false;
    }
}
