<?php
namespace ShopExpress\RfiBank\Injector;

use Psr\Log\NullLogger;
use Psr\Log\LoggerInterface;

trait LoggerInjector
{
    private $logger;

    public function getLogger()
    {
        if (!$this->logger) {
            $this->logger = new NullLogger();
        }
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }
}
