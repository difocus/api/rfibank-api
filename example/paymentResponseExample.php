<?php
require '../vendor/autoload.php';

use ShopExpress\RfiBank\Payment;

$serviceId = '';
$secret = '';

$d = new Payment($serviceId, $secret);
$data = $d->parseInput(); // возвращает данные с сервера RFIbank
$d->setCost(8);

if ($d->validate()) {
    echo 'Success Payment';
} else {
    echo 'Fail Payment';
}
