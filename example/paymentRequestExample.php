<?php
require '../vendor/autoload.php';

use ShopExpress\RfiBank\Payment;

$serviceId = '';
$secret = '';
$baseUrl = null;

$d = new Payment($serviceId, $secret, $baseUrl);

$d->setName('Килограмм помидоров');
$d->setComment('Килограмм отличных помидоров');
$d->setCost(8); // в рублях

echo $d->getPaymentForm();
